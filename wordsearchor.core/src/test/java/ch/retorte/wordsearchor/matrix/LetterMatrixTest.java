package ch.retorte.wordsearchor.matrix;

import ch.retorte.wordsearchor.file.WordListLoader;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link LetterMatrix}.
 */
public class LetterMatrixTest {

  @Test
  public void shouldImportTextBlock() {
    // given
    String textBlock =  "A  BC\n" +
                        "#wink\n" +
                        "   DEF   ";

    // when
    LetterMatrix letterMatrix = new LetterMatrix(convert(textBlock));

    // then
    assertThat(letterMatrix.toString(), is("ABC\nDEF"));
  }


  /***
   * Converts a text block to a list of text lines, removing all spaces.
   */
  private List<String> convert(String textBlock) {
    return new WordListLoader().convertTextBlock(textBlock);
  }
}
