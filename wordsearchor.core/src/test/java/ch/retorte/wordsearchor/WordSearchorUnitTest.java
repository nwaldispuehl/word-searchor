package ch.retorte.wordsearchor;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link WordSearchor}.
 */
public class WordSearchorUnitTest {

  //---- Fields




  //---- Test methods

  @Test
  public void shouldAcceptEmptyInput() {
    // given
    List<String> wordList = l();
    String input = "";
    
    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input);

    // then
    assertThat(result.getMatches().size(), is(0));
  }

  @Test(expected = IllegalArgumentException.class)
  public void shouldFailWithInvalidInput() {
    // given
    List<String> wordList = l();
    String input = "AB\nC";

    // when
    new WordSearchor(wordList).findMatchesIn(input);

    // then: Exception is expected.
  }

  @Test
  public void shouldFindTrivialMatch() {
    // given
    List<String> wordList = l("sun");
    String input = "S U N";

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input);

    // then
    assertThat(result.getMatches().size(), is(1));
    assertThat(result.getMatches().iterator().next().getWord(), containsString("sun"));
  }

  @Test
  public void shouldNotFindNoMatch() {
    // given
    List<String> wordList = l("xyz");
    String input = "A B C";

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input);

    // then
    assertThat(result.getMatches().size(), is(0));
  }

  @Test
  public void shouldNotFindNearMatch() {
    // given
    List<String> wordList = l("abd");
    String input = "A B C";

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input);

    // then
    assertThat(result.getMatches().size(), is(0));
  }

  @Test
  public void shouldFindMultiDirectionalMatch() {
    // given
    List<String> wordList = l("sun");
    String input =  "N X N X N\n" +
                    "X U U U X\n" +
                    "N U S U N\n" +
                    "X U U U X\n" +
                    "N X N X N";

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input);

    // then
    assertThat(result.getMatches().size(), is(8));
    assertThat(result.getMatches().iterator().next().getWord(), containsString("sun"));
  }

  @Test
  public void shouldIgnoreOneCharacterHits() {
    // given
    List<String> wordList = l("a", "b", "c");
    List<String> input = l("A B C");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 0);

    // then
    assertThat(result.getMatches().size(), is(0));
  }

  @Test
  public void shouldSkipTooShortMatches() {
    // given
    List<String> wordList = l("ab", "bc");
    List<String> input = l("A B C");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 3);

    // then
    assertThat(result.getMatches().size(), is(0));
  }

  @Test
  public void shouldEliminateDuplicates() {
    // given
    List<String> wordList = l("otto");
    List<String> input = l("A O T T O X");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 3);

    // then
    assertThat(result.getMatches().size(), is(1));
  }

  @Test
  public void shouldEliminateSubsets() {
    // given
    List<String> wordList = l("motor", "motorway", "way");
    List<String> input = l("M O T O R W A Y T O X", "X Y V W A Y P O I N T");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 3);

    // then
    assertThat(result.getMatches().size(), is(2));
  }

  @Test
  public void shouldNotEliminateIntersectingEntries() {
    // given
    List<String> wordList = l("rotor", "proto");
    List<String> input = l("R O T O R P");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 0);

    // then
    assertThat(result.getMatches().size(), is(2));
  }

  @Test
  public void shouldEliminateEarlierSmallerEntriesForwards() {
    // given
    List<String> wordList = l("motor", "tor");
    List<String> input = l("X M O T O R P");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 0);

    // then
    assertThat(result.getMatches().size(), is(1));
  }

  @Test
  public void shouldEliminateEarlierSmallerEntriesBackwards() {
    // given
    List<String> wordList = l("motor", "tor");
    List<String> input = l("X R O T O M P");

    // when
    WordSearchResult result = new WordSearchor(wordList).findMatchesIn(input, 0);

    // then
    assertThat(result.getMatches().size(), is(1));
  }


  //---- Helper methods

  private List<String> l(String... s) {
    return Arrays.asList(s);
  }
}
