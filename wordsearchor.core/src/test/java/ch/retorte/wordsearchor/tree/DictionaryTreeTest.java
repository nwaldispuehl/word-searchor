package ch.retorte.wordsearchor.tree;

import ch.retorte.wordsearchor.matrix.CharacterHolder;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link DictionaryTree}.
 */
public class DictionaryTreeTest {

  //---- Fields

  private DictionaryTree sut;


  //---- Test methods

  @Before
  public void setup() {
  }

  @Test
  public void shouldAcceptEmptyString() {
    // given
    List<String> wordList = l("");

    // when
    sut = new DictionaryTree(wordList);

    // then
    assertThat(sut.totalSize(), is(0));
  }

  @Test
  public void shouldAddStrings() {
    // given
    List<String> wordList = l("water", "walter");

    // when
    sut = new DictionaryTree(wordList);

    // then
    assertThat(sut.totalSize(), is(9));
    assertThat(sut.nodeFor(c("w")).get().nodeFor(c("a")).get().nodeFor(c("t")).get().nodeFor(c("e")).get().nodeFor(c("r")).get().word() , is("water"));
    assertThat(sut.nodeFor(c("w")).get().isTerminal(), is(false));
  }

  //---- Helper methods

  private List<String> l(String... s) {
    return Arrays.asList(s);
  }

  private CharacterHolder c(String s) {
    return () -> s.charAt(0);
  }
}
