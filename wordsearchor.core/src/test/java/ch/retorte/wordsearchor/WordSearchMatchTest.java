package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.matrix.Direction;
import ch.retorte.wordsearchor.matrix.Position;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link WordSearchMatch}.
 */
public class WordSearchMatchTest {

  @Test
  public void shouldIterateOverPositions() {
    // given
    WordSearchMatch sut = new WordSearchMatch(new Position(0, 0), new Position(0, 1), Direction.DOWN, "ab");
    List<Position> positionList = new ArrayList<>();

    // when
    sut.iterator().forEachRemaining(positionList::add);

    // then
    assertThat(positionList.size(), is(2));
  }

  @Test
  public void shouldIterateOverPositionsDiagonally() {
    // given
    WordSearchMatch sut = new WordSearchMatch(new Position(0, 0), new Position(5, 5), Direction.DOWN_RIGHT, "Hellow");
    List<Position> positionList = new ArrayList<>();

    // when
    sut.iterator().forEachRemaining(positionList::add);

    // then
    assertThat(positionList.size(), is(6));
  }
}
