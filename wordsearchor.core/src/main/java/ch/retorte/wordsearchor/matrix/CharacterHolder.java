package ch.retorte.wordsearchor.matrix;

/**
 * Holds a single character.
 */
public interface CharacterHolder {

  Character getCharacter();
}
