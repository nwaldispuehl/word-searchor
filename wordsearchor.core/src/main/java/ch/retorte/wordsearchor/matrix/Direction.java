package ch.retorte.wordsearchor.matrix;

import static ch.retorte.wordsearchor.matrix.Translation.translateBy;

/**
 * Denotes a search direction.
 */
public enum Direction {

  //---- Enum values

  UP(translateBy(0, -1)),
  UP_RIGHT(translateBy(1, -1)),
  RIGHT(translateBy(1, 0)),
  DOWN_RIGHT(translateBy(1, 1)),
  DOWN(translateBy(0, 1)),
  DOWN_LEFT(translateBy(-1, 1)),
  LEFT(translateBy(-1, 0)),
  UP_LEFT(translateBy(-1, -1));

  //---- Static

  static {
    UP.opposite           = DOWN;
    UP_RIGHT.opposite     = DOWN_LEFT;
    RIGHT.opposite        = LEFT;
    DOWN_RIGHT.opposite   = UP_LEFT;
    DOWN.opposite         = UP;
    DOWN_LEFT.opposite    = UP_RIGHT;
    LEFT.opposite         = RIGHT;
    UP_LEFT.opposite      = DOWN_RIGHT;
  }

  //---- Fields

  private final Translation translation;
  private Direction opposite;


  //---- Constructor

  Direction(Translation translation) {
    this.translation = translation;
  }


  //---- Methods

  public Position translate(Position position) {
    return translation.translate(position);
  }

  public boolean isSameAs(Direction direction) {
    return this == direction || this.opposite == direction;
  }
}
