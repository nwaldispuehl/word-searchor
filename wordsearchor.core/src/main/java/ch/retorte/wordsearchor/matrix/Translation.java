package ch.retorte.wordsearchor.matrix;

/**
 * Describes a position translation.
 */
class Translation {

  //---- Fields

  private final int xDisplacement;
  private final int yDisplacement;


  //---- Constructor

  private Translation(int xDisplacement, int yDisplacement) {
    this.xDisplacement = xDisplacement;
    this.yDisplacement = yDisplacement;
  }


  //---- Static methods

  static Translation translateBy(int xDisplacement, int yDisplacement) {
    return new Translation(xDisplacement, yDisplacement);
  }


  //---- Methods

  Position translate(Position position) {
    return new Position(position.x() + xDisplacement, position.y() + yDisplacement);
  }

}
