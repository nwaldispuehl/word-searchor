package ch.retorte.wordsearchor.matrix;

/**
 * A point on the plane.
 */
public final class Position {

  //---- Fields

  private final int x;
  private final int y;


  //---- Constructor

  public Position(int x, int y) {
    this.x = x;
    this.y = y;
  }


  //---- Methods

  int x() {
    return x;
  }

  int y() {
    return y;
  }

  @Override
  public String toString() {
    return x + "," + y;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Position) {
      Position otherPos = (Position) obj;
      return x == otherPos.x && y == otherPos.y;
    }
    else {
      return super.equals(obj);
    }
  }

  @Override
  public int hashCode() {
    return new Integer(x).hashCode() ^ new Integer(y).hashCode();
  }
}
