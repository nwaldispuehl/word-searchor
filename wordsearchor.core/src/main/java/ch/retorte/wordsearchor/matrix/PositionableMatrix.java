package ch.retorte.wordsearchor.matrix;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;

/**
 * Parameterizable matrix which prevents to deal with these nasty coordinates.
 */
public class PositionableMatrix<T> implements Iterable<PositionableMatrix.ItemHolder<T>> {

  //---- Fields

  private final int width;
  private final int height;
  private PositionableMatrix.ItemHolder<T>[][] data;


  //---- Constructor

  public PositionableMatrix(int width, int height) {
    this.width = width;
    this.height = height;

    initialize();
  }


  //---- Methods

  private void initialize() {
    data = new PositionableMatrix.ItemHolder[width][height];

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        data[x][y] = new ItemHolder<>(new Position(x ,y));
      }
    }
  }

  public T get(Position position) {
    return getItemHolderFor(position).getData();
  }

  private ItemHolder<T> getItemHolderFor(Position position) {
    return data[position.x()][position.y()];
  }

  public void put(Position position, T t) {
    data[position.x()][position.y()].setData(t);
  }


  @Override
  public Iterator<PositionableMatrix.ItemHolder<T>> iterator() {
    return new Iterator<PositionableMatrix.ItemHolder<T>>() {

      private int currentX = 0;
      private int currentY = 0;

      @Override
      public boolean hasNext() {
        return currentX < width && currentY < height;
      }

      @Override
      public PositionableMatrix.ItemHolder<T> next() {
        Position position = new Position(currentX, currentY);

        if (currentX < width - 1) {
          currentX = currentX + 1;
        }
        else {
          currentX = 0;
          currentY = currentY + 1;
        }

        return getItemHolderFor(position);
      }
    };
  }

  public Iterator<ItemLine> lineIterator() {
    return new Iterator<ItemLine>() {

      int currentLine = 0;

      @Override
      public boolean hasNext() {
        return currentLine < height;
      }

      @Override
      public ItemLine next() {
        List<ItemHolder> items = new LinkedList<>();
        for (int x = 0; x < width; x++) {
          items.add(data[x][currentLine]);
        }

        currentLine++;
        return new ItemLine(items);
      }
    };
  }

  @Override
  public String toString() {
    StringJoiner sj = new StringJoiner("\n");
    for (int y = 0; y < height; y++) {
      StringBuilder sb = new StringBuilder();
      for (int x = 0; x < width; x++) {
        sb.append(data[x][y].getData());
      }
      sj.add(sb.toString());
    }
    return sj.toString();
  }

  //---- Inner classes

  public static class ItemHolder<U> {

    private Position position;
    private U data;

    private ItemHolder(Position position) {
      this.position = position;
    }

    public Position getPosition() {
      return position;
    }

    void setData(U data) {
      this.data = data;
    }

    public U getData() {
      return data;
    }
  }

  public class ItemLine {

    private List<ItemHolder> items;

    ItemLine(List<ItemHolder> items) {
      this.items = items;
    }

    public List<ItemHolder> getItems() {
      return items;
    }
  }

}
