package ch.retorte.wordsearchor.matrix;

import ch.retorte.wordsearchor.file.WordListLoader;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.joining;

/**
 * The letter matrix holds the input data.
 */
public class LetterMatrix implements Iterable<LetterMatrix.MatrixItem> {

  private int width = 0;
  private int height = 0;

  private PositionableMatrix<Character> data;

  public LetterMatrix(List<String> lines) {
    initializeWith(lines);
  }

  private void initializeWith(List<String> rawLines) {
    List<String> lines = cleanUp(rawLines);
    validate(lines);

    if (lines.isEmpty() || lines.get(0).isEmpty()) {
      data = new PositionableMatrix<>(0, 0);
      return;
    }


    width = lines.get(0).length();
    height = lines.size();

    data = new PositionableMatrix<>(width, height);

    for (int y = 0; y < height; y++) {
      String line = lines.get(y);
      for (int x = 0; x < width; x++) {
        data.put(new Position(x, y), line.charAt(x));
      }
    }
  }

  private List<String> cleanUp(List<String> rawLines) {
    return new WordListLoader().cleanUp(rawLines);
  }

  /**
   * All lines need to have the same length.
   */
  private void validate(List<String> lines) {
    if (!lines.isEmpty()) {
      int length = lines.get(0).length();
      for (String s : lines) {
        if (length != s.length()) {
          throw new IllegalArgumentException("Every line must have the same number of characters. Provided input: \n" + join(lines));
        }
      }
    }
  }

  private String join(List<String> lines) {
    return lines.stream().collect(joining(System.lineSeparator()));
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  private boolean isValid(Position position) {
    int x = position.x();
    int y = position.y();
    return 0 <= x && x < width &&  0 <= y && y < height;
  }

  private Character getFor(Position position) {
    return data.get(position);
  }

  @Override
  public Iterator<MatrixItem> iterator() {
    return new Iterator<MatrixItem>() {

      private int currentX = 0;
      private int currentY = 0;

      @Override
      public boolean hasNext() {
        return currentX < width && currentY < height;
      }

      @Override
      public MatrixItem next() {
        Position position = new Position(currentX, currentY);

        if (currentX < width - 1) {
          currentX = currentX + 1;
        }
        else {
          currentX = 0;
          currentY = currentY + 1;
        }

        return new MatrixItem(position, getFor(position));
      }
    };
  }

  @Override
  public String toString() {
    return data.toString();
  }

  //---- Inner classes

  /**
   * Denotes a single item of the {@link LetterMatrix}.
   */
  public class MatrixItem implements CharacterHolder {

    private Position position;
    private Character character;

    private MatrixItem(Position position, Character character) {
      this.position = position;
      this.character = character;
    }

    public Position position() {
      return position;
    }

    public Optional<MatrixItem> getNextIn(Direction direction) {
      Position nextPosition = direction.translate(position);
      if (isValid(nextPosition)) {
        return Optional.of(new MatrixItem(nextPosition, getFor(nextPosition)));
      }
      else {
        return Optional.empty();
      }
    }

    @Override
    public Character getCharacter() {
      return character;
    }
  }


}
