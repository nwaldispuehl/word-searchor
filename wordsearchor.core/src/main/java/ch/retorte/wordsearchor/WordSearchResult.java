package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.matrix.LetterMatrix;
import ch.retorte.wordsearchor.matrix.Position;
import ch.retorte.wordsearchor.matrix.PositionableMatrix;

import java.util.*;

/**
 * Holds information about the result of a word search.
 */
public class WordSearchResult {

  //---- Static




  //---- Fields

  private LetterMatrix input;
  private Collection<WordSearchMatch> matches = new LinkedList<>();
  private PositionableMatrix<MatchData> data;
  private int minimumMatchLength;


  //---- Constructor

  WordSearchResult(LetterMatrix letterMatrix, int minimumMatchLength) {
    this.input = letterMatrix;
    this.minimumMatchLength = minimumMatchLength;
    initializeMatrixWith(letterMatrix);
  }


  //---- Methods

  private void initializeMatrixWith(LetterMatrix letterMatrix) {
    data = new PositionableMatrix<>(letterMatrix.getWidth(), letterMatrix.getHeight());

    for (LetterMatrix.MatrixItem item : letterMatrix) {
      data.put(item.position(), new MatchData(item.getCharacter()));
    }
  }

  public String getInput() {
    return input.toString();
  }

  public Collection<WordSearchMatch> getMatches() {
    return matches;
  }

  public int size() {
    return getMatches().size();
  }

  public Iterator<PositionableMatrix<WordSearchResult.MatchData>.ItemLine> getMatchLines() {
   return data.lineIterator();
  }

  void add(WordSearchMatch match) {
    if (isValid(match)) {
      matches.add(match);
      match.forEach(p -> data.get(p).add(match));
      removeDuplicatesOf(match);
    }
  }

  private void remove(WordSearchMatch match) {
    matches.remove(match);
    match.forEach(p -> data.get(p).remove(match));
  }

  private boolean isValid(WordSearchMatch match) {
    return match != null && minimumMatchLength <= match.length();
  }

  /**
   * A match is a duplicate if its letters are identical to an already existing entry. This occurs with palindromes or smaller words overlapping larger ones.
   */
  private void removeDuplicatesOf(WordSearchMatch match) {
    Set<WordSearchMatch> duplicateCandidates = new HashSet<>();

    for (Position p : match) {
      MatchData matchData = data.get(p);
      duplicateCandidates.addAll(matchData.getMatchesWithSameDirectionAs(match));
    }

    // We first check which candidates are dominated by the current match, and remove them.
    for (WordSearchMatch m : new HashSet<>(duplicateCandidates)) {
      if (match.dominates(m)) {
        duplicateCandidates.remove(m);
        remove(m);
      }
    }

    // Then we check if the current match is dominated by any candidates. If yes we remove it again.
    for (WordSearchMatch m : duplicateCandidates) {
      if (m.dominates(match)) {
        remove(match);
      }
    }
  }

  //---- Inner class

  public class MatchData {

    private Character character;
    private Collection<WordSearchMatch> matches = new LinkedList<>();

    private MatchData(Character character) {
      this.character = character;
    }

    private void add(WordSearchMatch match) {
      this.matches.add(match);
    }

    private void remove(WordSearchMatch match) {
      this.matches.remove(match);
    }

    private Set<WordSearchMatch> getMatchesWithSameDirectionAs(WordSearchMatch match) {
      Set<WordSearchMatch> result = new HashSet<>();
      for (WordSearchMatch m : matches) {
        if (m != match && m.getDirection().isSameAs(match.getDirection())) {
          result.add(m);
        }
      }
      return result;
    }

    public boolean hasMatch() {
      return !matches.isEmpty();
    }

    public Character getCharacter() {
      return character;
    }
  }


}
