package ch.retorte.wordsearchor.tree;

import ch.retorte.wordsearchor.matrix.CharacterHolder;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * The {@link DictionaryTree} is a data structure which holds all words of a dictionary in a tree-like structure which allows to traverse them.
 */
public class DictionaryTree {

  //---- Fields

  private TreeNode rootNode = new TreeNode();


  //---- Constructor

  public DictionaryTree(Collection<String> wordList) {
    initializeWith(wordList);
  }


  //---- Methods

  private void initializeWith(Collection<String> wordList) {
    for (String word : wordList) {
      add(word);
    }
  }

  private void add(String word) {
    if (word.isEmpty()) {
      return;
    }

    rootNode.add(word, word);
  }

  public Optional<TreeNode> nodeFor(CharacterHolder characterHolder) {
    return rootNode.nodeFor(characterHolder);
  }

  int totalSize() {
    return rootNode.totalSize();
  }


  //---- Inner classes

  public class TreeNode {

    //---- Fields

    private Character character;
    private String word;
    private List<TreeNode> nodes = new LinkedList<>();


    //---- Constructor

    /**
     * Empty constructor used for the root node.
     */
    private TreeNode() {}

    private TreeNode(Character character, String word, String remainder) {
      this.character = character;
      add(word, remainder);
    }

    //---- Methods

    public boolean isTerminal() {
      return word != null;
    }

    public String word() {
      return word;
    }

    public Optional<TreeNode> nodeFor(CharacterHolder characterHolder) {
      return Optional.ofNullable(nodeFor(characterHolder.getCharacter()));
    }

    private TreeNode nodeFor(Character character) {
      for (TreeNode n : nodes) {
        if (n.has(character)) {
          return n;
        }
      }
      return null;
    }

    private boolean has(Character character) {
      return Character.toLowerCase(this.character) == Character.toLowerCase(character);
    }

    private void add(String word, String remainder) {
      if (remainder.isEmpty()) {
        this.word = word;
      }
      else {
        Character first = remainder.charAt(0);
        String rest = remainder.substring(1);

        TreeNode node = nodeFor(first);
        if (node != null) {
          node.add(word, rest);
        }
        else {
          node = new TreeNode(first, word, rest);
          nodes.add(node);
        }
      }
    }

    int totalSize() {
      int totalSize = nodes.size();
      for (TreeNode n : nodes) {
        totalSize += n.totalSize();
      }
      return totalSize;
    }


  }

}
