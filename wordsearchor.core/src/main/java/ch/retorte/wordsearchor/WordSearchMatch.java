package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.matrix.Direction;
import ch.retorte.wordsearchor.matrix.Position;

import java.util.Iterator;

/**
 * Holds data about a word match.
 */
public class WordSearchMatch implements Iterable<Position> {

  //---- Fields

  private Direction direction;
  private String word;
  private Position start;
  private Position end;


  //---- Constructor

  WordSearchMatch(Position start, Position end, Direction direction, String word) {
    this.start = start;
    this.end = end;
    this.direction = direction;
    this.word = word;
  }


  //---- Methods

  public Position getStart() {
    return start;
  }

  public Position getEnd() {
    return end;
  }

  public String getWord() {
    return word;
  }

  public Direction getDirection() {
    return direction;
  }

  public int length() {
    return word.length();
  }

  public boolean covers(Position position) {
    for (Position p : this) {
      if (p.equals(position)) {
        return true;
      }
    }
    return false;
  }

  /**
   * We are dominating another match if all of their letters are enclosed in this match.
   */
  public boolean dominates(WordSearchMatch other) {
    for (Position p : other) {
      if (!covers(p)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return word + " (" + start + ") -> (" + end +")";
  }

  @Override
  public Iterator<Position> iterator() {
    return new Iterator<Position>() {
      private Position currentPosition = null;

      @Override
      public boolean hasNext() {
        return !getEnd().equals(currentPosition);
      }

      @Override
      public Position next() {
        if (currentPosition != null) {
          currentPosition = getDirection().translate(currentPosition);
        }
        else {
          currentPosition = getStart();
        }

        return currentPosition;
      }
    };
  }
}
