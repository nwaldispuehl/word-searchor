package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.file.WordListLoader;
import ch.retorte.wordsearchor.matrix.Direction;
import ch.retorte.wordsearchor.matrix.LetterMatrix;
import ch.retorte.wordsearchor.tree.DictionaryTree;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * The {@link WordSearchor} performs the search algorithm.
 */
public class WordSearchor {

  //---- Static

  private static final int DEFAULT_MINIMUM_LENGTH = 3;


  //---- Fields

  private DictionaryTree dictionaryTree;


  //---- Constructors

  public WordSearchor(Collection<String> wordList) {
    this(new DictionaryTree(wordList));
  }

  public WordSearchor(DictionaryTree dictionaryTree) {
    this.dictionaryTree = dictionaryTree;
  }


  //---- Methods

  public WordSearchResult findMatchesIn(String input) {
    return findMatchesIn(input, DEFAULT_MINIMUM_LENGTH);
  }

  /***
   * Converts a text block to a list of text lines, removing all spaces.
   */
  private List<String> convert(String textBlock) {
    return new WordListLoader().convertTextBlock(textBlock);
  }

  public WordSearchResult findMatchesIn(String input, int minimumLength) {
    return findMatchesIn(convert(input), minimumLength);
  }

  public WordSearchResult findMatchesIn(List<String> lines, int minimumLength) {
    LetterMatrix letterMatrix = new LetterMatrix(lines);
    return findMatchesIn(letterMatrix, minimumLength);
  }

  private WordSearchResult findMatchesIn(LetterMatrix letterMatrix, int minimumLength) {
    WordSearchResult result = new WordSearchResult(letterMatrix, minimumLength);

    for (LetterMatrix.MatrixItem item : letterMatrix) {
      Optional<DictionaryTree.TreeNode> node = dictionaryTree.nodeFor(item);
      if (node.isPresent()) {

        for (Direction direction : Direction.values()) {
          WordSearchMatch match = findWith(item, item, node.get(), direction);
          result.add(match);
        }
      }
    }

    return result;
  }

  private WordSearchMatch findWith(LetterMatrix.MatrixItem startItem, LetterMatrix.MatrixItem currentItem, DictionaryTree.TreeNode node, Direction direction) {
    Optional<LetterMatrix.MatrixItem> nextItem = currentItem.getNextIn(direction);
    if (nextItem.isPresent()) {
      Optional<DictionaryTree.TreeNode> nextNode = node.nodeFor(nextItem.get());

      if (nextNode.isPresent()) {
        WordSearchMatch match = findWith(startItem, nextItem.get(), nextNode.get(), direction);
        if (match != null) {
          return match;
        }
      }
    }

    if (node.isTerminal() && !areOnSamePosition(startItem, currentItem)) {
      return new WordSearchMatch(startItem.position(), currentItem.position(), direction, node.word());
    }

    return null;
  }

  private boolean areOnSamePosition(LetterMatrix.MatrixItem i1, LetterMatrix.MatrixItem i2) {
    return i1.position().equals(i2.position());
  }

}
