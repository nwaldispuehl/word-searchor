package ch.retorte.wordsearchor.file;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for the {@link WordListLoader}.
 */
public class WordListLoaderTest {

  //---- Fields

  private WordListLoader sut = new WordListLoader();


  //---- Test methods

  @Test
  public void shouldProduceWordListParts() {
    // given
    Stream<String> wordStream = streamOf("my", "fancy", "list");

    // when
    List<String> filteredWordList = sut.getFilteredWordListFrom(wordStream);

    // then
    assertThat(filteredWordList, is(l("my", "fancy", "list")));
  }

  @Test
  public void shouldFilterUnwantedParts() {
    // given
    Stream<String> wordStream = streamOf("Hoho", null, "", "# Whee, comment", "     # Yet another comment", "nice");

    // when
    List<String> filteredWordList = sut.getFilteredWordListFrom(wordStream);

    // then
    assertThat(filteredWordList, is(l("Hoho", "nice")));
  }

  @Test
  public void shouldConvertTextBlock() {
    // given
    String textBlock =  "A B C\n" +
                        "# Nice comment.\n" +
                        "\n" +
                        "de     f\n";

    // when
    List<String> lines = sut.convertTextBlock(textBlock);

    // then
    assertThat(lines.size(), is(2));
    assertThat(lines.get(0), is("ABC"));
    assertThat(lines.get(1), is("def"));
  }


  //---- Helper methods

  private Stream<String> streamOf(String... s) {
    return Arrays.stream(s);
  }

  private List<String> l(String... s) {
    return Arrays.asList(s);
  }

}
