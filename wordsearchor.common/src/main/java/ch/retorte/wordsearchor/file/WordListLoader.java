package ch.retorte.wordsearchor.file;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * Util to load word lists from files in the class path.
 */
public class WordListLoader {

  //---- Static

  private static final String COMMENT_PREFIX = "#";
  private static final String SPACE = " ";
  private static final String EMPTY_STRING = "";

  /* We're using the new line character class '\R'. */
  private static final String NEW_LINE_REGEX = "\\R";

  //---- Methods

  /**
   * Converts the given resource (which is assumed to hold a word on every line) into a list of strings.
   * Empty lines and comments (i.e. lines starting with the hash ('#')) are ignored.
   */
  public List<String> asList(String resourcePath) {
    InputStream wordListStream = getInputStreamFromResource(resourcePath);
    Stream<String> lineStream = getLineStreamFrom(wordListStream);
    return getFilteredWordListFrom(lineStream);
  }

  /**
   * Converts a given resource path to a file.
   */
  public InputStream getInputStreamFromResource(String resourceFilePath) {
    return getClass().getResourceAsStream(resourceFilePath);
  }

  public File getFileFromResource(String resourceFilePath) {
    URL resource = getClass().getResource(resourceFilePath);
    return new File(resource.getFile());
  }

  public InputStream getInputStreamFromFile(File file) throws FileNotFoundException {
    return new FileInputStream(file);
  }

  public List<String> asList(InputStream inputStream) {
    Stream<String> lineStream = getLineStreamFrom(inputStream);
    return getFilteredWordListFrom(lineStream);
  }

  private Stream<String> getLineStreamFrom(InputStream inputStream) {
    return new BufferedReader(new InputStreamReader(inputStream)).lines();
  }

  List<String> getFilteredWordListFrom(Stream<String> wordStream) {
    return wordStream.filter(this::isRelevant).collect(toList());
  }

  private boolean isRelevant(String line) {
    return !isEmpty(line) && !isComment(line);
  }

  public String clean(String line) {
    return line.replaceAll(SPACE, EMPTY_STRING).trim();
  }

  private boolean isEmpty(String line) {
    return line == null || line.trim().isEmpty();
  }

  private boolean isComment(String line) {
    return line.trim().startsWith(COMMENT_PREFIX);
  }

  /**
   * Converts a block of text (containing lines and line feed characters) into a list of
   * single lines, sans spaces and stuff.
   */
  public List<String> convertTextBlock(String textBlock) {
    return Arrays.stream(clean(textBlock).split(NEW_LINE_REGEX)).filter(this::isRelevant).collect(toList());
  }

  public List<String> cleanUp(List<String> lines) {
    return lines.stream().map(this::clean).filter(this::isRelevant).collect(toList());
  }
}
