package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.file.WordListLoader;
import ch.retorte.wordsearchor.spi.LanguageDictionary;

import java.io.InputStream;
import java.util.List;

/**
 * Language dictionary which is based on a text file. The said text file contains one word per line.
 */
public class InputStreamBasedLanguageDictionary implements LanguageDictionary {

  //---- Fields

  private final List<String> wordList;


  //---- Constructor

  public InputStreamBasedLanguageDictionary(InputStream wordListStream) {
    try {
      wordList = new WordListLoader().asList(wordListStream);
    }
    catch (Exception e) {
      throw new RuntimeException("Was not able to load dictionary from input stream: " + wordListStream, e);
    }
  }


  //---- Methods

  @Override
  public List<String> wordList() {
    return wordList;
  }

}
