package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.file.WordListLoader;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Language dictionary which is based on a text file. The said text file contains one word per line.
 */
public class FileBasedLanguageDictionary extends InputStreamBasedLanguageDictionary {

  //---- Constructor

  public FileBasedLanguageDictionary(File wordListFile) throws FileNotFoundException {
    super(new WordListLoader().getInputStreamFromFile(wordListFile));
  }

}
