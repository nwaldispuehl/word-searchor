package ch.retorte.wordsearchor.spi;

import java.util.List;

/**
 * A {@link LanguageDictionary} offers a more of less exhaustive collection of words of a given language.
 */
public interface LanguageDictionary {

  /**
   * Produces a word list of the given implementation (i.e. language). Note that the words are supposed to be ordered, e.g. in order of their occurrence probability.
   */
  List<String> wordList();

}
