# Word Searchor

_Solver for word search puzzles._

## Introduction

The _Word Searchor_ is a dictionary-based solver for word puzzles. It takes puzzles like this as input:
 
    I R K H U N D U O H T
    F X Y S J Q M R V Y R
    I N T R I C K L E N O  
    W F Y D P X O T X A U
    O A M Q D I S H D G T
    
and produces output like this:

    - Word list (1 hits):
    trickle (2,2) -> (8,2)
      
    - Output:
    . . . . . . . . . . .
    . . . . . . . . . . .
    . . T R I C K L E . .
    . . . . . . . . . . .
    . . . . . . . . . . .

It has a small number of built-in word lists but can be provided with arbitrary lists. It is available as command line program. 

##### Table of Contents  

* [How does it work?](#how_does_it_work)  
  * [Input](#input) 
    * [Text input](#input_text)
        * [Input file](#input_text_files)
        * [Stdin](#input_text_stdin)
    * [Image input](#input_image)
  * [Dictionary](#dictionary)
    * [Using built in dictionaries](#dictionary_builtin)
    * [Providing own dictionary](#dictionary_own)
  * [Minimum word size](#minimum_word_size) 
* [How to download?](#how_to_download)  
* [How to build and run?](#how_to_build_and_run)  

<a name='how_does_it_work' />

## How does it work?

When started with the `-h` (help) switch like this:
```
$ ./bin/wordsearchor -h
```

the help screen is shown:

    Word Searchor -- Solver for word search puzzles.
     
    Solves classical word puzzles - where words have to be found in a block of
    letters - with text or an image as input data. Has already some default 
    dictionaries included, custom word lists can be provided
     
    Input has to be present in block form, e.g.
     
        A B C
        D E F
     
    usage: wordsearchor [OPTIONS] [INPUTFILE]
                        The INPUTFILE can either be a text or image file,
                        or none. Then input is read from stdin.
     -s,--min-word-size    Minimum length (inclusive) of words to look for.
                           Defaults to 4.
     -d,--dict             Path to a custom dictionary in a text file. One
                           word per line.
     -h,--help             Displays the help screen.
     -l,--lang             Which language dictionary to use. Supports 'en',
                           'de', 'fr', 'es'. Defaults to 'en'.

In the following we will have a look at the options and arguments.

<a name='input' />

### Input

<a name='input_text' />

#### Text input

<a name='input_text_files' />

##### Input files

When providing an input file as argument, the _Word Searchor_ loads its content and uses it as input for the word search. Assuming we have a file `input.txt` with the following contents:

    I R K H U N D U O H T
    F X Y S J Q M R V Y R
    I N T R I C K L E N O  
    W F Y D P X O T X A U
    O A M Q D I S H D G T

Then calling this command would process it:
 
```
$ ./bin/wordsearchor input.txt
```
and yield this output:

    . . . . . . . . . . T
    . . . . . . . . . . R
    . . T R I C K L E . O
    . . . . . . . . . . U
    . . . . D I S H . . T

By using the `-v` (verbose) switch, we can display more information:

```
$ ./bin/wordsearchor -v input.txt
```
    - Input:
    I R K H U N D U O H T
    F X Y S J Q M R V Y R
    I N T R I C K L E N O  
    W F Y D P X O T X A U
    O A M Q D I S H D G T
      
    - Word list (3 hits):
    trout (10,0) -> (10,4)
    trickle (2,2) -> (8,2)
    dish (4,4) -> (7,4)
      
    - Output:
    . . . . . . . . . . T
    . . . . . . . . . . R
    . . T R I C K L E . O
    . . . . . . . . . . U
    . . . . D I S H . . T



<a name='input_text_stdin' />

##### Stdin

If no input file argument is provided the _Word Searchor_ waits for text input on the `stdin` of the console. You can use it to pipe word puzzles through it, for example like this:

```
$ cat puzzle.txt | ./bin/wordsearchor
```
The output is:

    . . . . . . . . . . T
    . . . . . . . . . . R
    . . T R I C K L E . O
    . . . . . . . . . . U
    . . . . D I S H . . T

<a name='input_image' />

#### Image input

If you provide an image file instead of a text file as input file argument, _Word Searchor_ attempts to apply _optical character recognition_ (OCR) on it to extract the contained word puzzle.

_Note: Not yet implemented._

<a name='dictionary' />

### Dictionary

The _Word Searchor_ uses word lists to find matches in the word puzzles. It thus can't identify any words not existing in the currently used word list. It has already some word lists included and uses the english one as default if none is specified.

<a name='dictionary_builtin' />

#### Using built in dictionaries

The currently available list of built-in dictionaries is shown in the help screen:

     -l,--lang         Which language dictionary to use. Supports 'en',
                       'de', 'fr', 'es'. Defaults to 'en'.

These contain between 30K and 100K words. To use the german one on our example start the software like this:

```
$ ./bin/wordsearchor -l de -v input.txt
```
The output is:

    - Input:
    I R K H U N D U O H T
    F X Y S J Q M R V Y R
    I N T R I C K L E N O  
    W F Y D P X O T X A U
    O A M Q D I S H D G T
      
    - Word list (2 hits):
    Hund (3,0) -> (6,0)
    Trick (2,2) -> (6,2)
      
    - Output:
    . . . H U N D . . . .
    . . . . . . . . . . .
    . . T R I C K . . . .
    . . . . . . . . . . .
    . . . . . . . . . . .


<a name='dictionary_own' />

#### Providing own dictionary

You also could use your own dictionary. It has to be present in a text file containing one word per line. Lets assume we have a file `myDictionary.txt` with the following content:

    Rick
    Leno
    Wifi
    
We then can reference it in the call with the `-d` option:

```
$ ./bin/wordsearchor -d myDictionary.txt input.txt
```
The output is then:

    I . . . . . . . . . .
    F . . . . . . . . . .
    I . . R I C K L E N O
    W . . . . . . . . . .
    . . . . . . . . . . .

<a name='minimum_word_size' />

### Minimum word size

The `-s` (size) flag determines the minimum length of words we consider from the dictionary. Its default is 4. If we would like to have more trivial word matches we decrease the value to for example 3:

```
$ ./bin/wordsearchor -s 3 -v input.txt
```
    - Input:
    I R K H U N D U O H T
    F X Y S J Q M R V Y R
    I N T R I C K L E N O 
    W F Y D P X O T X A U
    O A M Q D I S H D G T
      
    - Word list (13 hits):
    irk (0,0) -> (2,0)
    duo (6,0) -> (8,0)
    trout (10,0) -> (10,4)
    six (3,1) -> (5,3)
    MCP (6,1) -> (4,3)
    vex (8,1) -> (8,3)
    trickle (2,2) -> (8,2)
    eat (8,2) -> (10,4)
    nag (9,2) -> (9,4)
    one (10,2) -> (8,2)
    Drs (3,3) -> (3,1)
    pry (4,3) -> (2,1)
    dish (4,4) -> (7,4)
      
    - Output:
    I R K . . . D U O . T
    . . Y S . . M . V . R
    . . T R I C K L E N O
    . . . D P X . . X A U
    . . . . D I S H . G T


<a name='how_to_download' />

## How to download?

If you just want to use it, download the latest release from the release directory:

[Word-Searchor tags](https://gitlab.com/nwaldispuehl/word-searchor/tags)

Unpack the archive (adapt version if needed) and start the application as follows on Linux and OS X operating systems. Certainly, you need a installed [Java](http://www.java.com/) runtime (>= 1.8) on your system. On Microsoft Windows systems just use the .bat file.

```
$ unzip wordsearchor-0.0.1.zip
$ cd wordsearchor-0.0.1/
$ ./bin/wordsearchor -h
``` 
(The `-h` switch shows the help screen.)

<a name='how_to_build_and_run' />

## How to build and run?


Clone the repository and change into the cloned project:
```
$ git clone https://gitlab.com/nwaldispuehl/word-searchor.git
$ cd word-searchor
``` 

Build the project with [**gradle**](http://www.gradle.org/). Either use your own installation, or use the shipped version as shown here. Create a zipped distribution as follows:
```
word-searchor$ ./gradlew clean distZip
``` 
and find the distribution in the respective build directory:
```
word-searchor$ ls wordsearchor.cli/build/distributions/
wordsearchor-0.0.1.zip
```
Or build the runnable version like this:
```
word-searchor$ ./gradlew clean installDist
```
Switch into the build directory and run the installed app:
```
word-searchor$ cd wordsearchor.cli/build/install/wordsearchor/
word-searchor/wordsearchor.cli/build/install/wordsearchor$ ./bin/wordsearchor -h
``` 
(The `-h` switch shows the help screen.)

Certainly, you could run the project also directly with gradle, but providing command line arguments is a bit cumbersome this way:
```
word-searchor$ ./gradlew run
```


