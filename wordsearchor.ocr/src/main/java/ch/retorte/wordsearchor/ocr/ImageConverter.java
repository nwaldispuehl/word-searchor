package ch.retorte.wordsearchor.ocr;

import ch.retorte.wordsearchor.ocr.client.TesseractClient;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Attempts to convert a provided image to plain text.
 */
public class ImageConverter {

  //---- Fields

  private TesseractClient tesseractClient = new TesseractClient();

  //---- Methods

  public String convert(File imageFile) throws IOException, URISyntaxException {
    return tesseractClient.retrieveContentFrom(imageFile);
  }

}
