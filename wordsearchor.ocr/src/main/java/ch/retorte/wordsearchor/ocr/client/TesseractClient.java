package ch.retorte.wordsearchor.ocr.client;

import ch.retorte.wordsearchor.file.WordListLoader;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

/**
 * Acts as a user friendly front end for the Tesseract library code.
 *
 * To use it, you can use '{@link TesseractClient#retrieveContentFrom(File)}' to try to extract text from an image.
 */
public class TesseractClient {

  //---- Static

  private static final String TRAINIG_FILE = "/tesseract_trainingdata/ENG.traineddata";

  //---- Fields

  private WordListLoader fileLoader = new WordListLoader();

  //---- Methods

  /**
   * Tries to extract text from the provided input image file according to the trained images.
   * Note that we assume the input image to be very clean.
   */
  public String retrieveContentFrom(File inputFile) throws IOException {
    InputStream inputStream = fileLoader.getInputStreamFromResource(TRAINIG_FILE);

    String temporaryDirectory = System.getProperty("java.io.tmpdir");
    File whee = new File(temporaryDirectory + File.separator + "ENG.traineddata");
    Files.copy(inputStream, whee.toPath(), StandardCopyOption.REPLACE_EXISTING);

    tesseract.TessBaseAPI tesseract = new tesseract.TessBaseAPI();
    if (tesseract.Init(temporaryDirectory, "ENG") != 0) {
      System.err.println("Could not initialize tesseract.");
      System.exit(1);
    }

    lept.PIX inputImage = pixRead(inputFile.getAbsolutePath());
    tesseract.SetImage(inputImage);

    BytePointer resultPointer = tesseract.GetUTF8Text();
    String result = resultPointer.getString();

    tesseract.End();
    resultPointer.deallocate();
    pixDestroy(inputImage);

    whee.delete();

    return result.trim();
  }

}
