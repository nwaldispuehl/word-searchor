package ch.retorte.wordsearchor.ocr;

import ch.retorte.wordsearchor.file.WordListLoader;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link ImageConverter}.
 */
public class ImageConverterTest {

  //---- Fields

  private ImageConverter sut = new ImageConverter();

  //---- Tests

  @Test
  public void shouldConvertHappyCase() throws Exception {
    // given
    File input = new WordListLoader().getFileFromResource("/sample_happy_case.png");

    // when
    String output = sut.convert(input);

    // then
    assertThat(tidied(output), is("ABC\nDEF"));
  }

  @Test
  public void shouldConvertLargeRealLifeCase() throws Exception {
    // given
    File input = new WordListLoader().getFileFromResource("/sample_word_puzzle.jpg");

    // when
    String output = sut.convert(input);

    // then
    assertThat(tidied(output), is("ZICGPQOJBQNCYNFQEJJS\n" +
                                  "JONSNVPZHAOTJOLBXHKU\n" +
                                  "QJTNAIDQCMISFIHXCLNP\n" +
                                  "GDDKOENAXSYWFTRMEHXM\n" +
                                  "ZRQAEVDRRNXDXAUPLCDA\n" +
                                  "DXOCZEAEAXWOZRYQLZVC\n" +
                                  "FYCWIVIAVTDEVELOPMENTY\n" +
                                  "XUMIVIJNIWLFYBPQNDNM\n" +
                                  "SVEBNSRXHOXUGAXXCJXS\n" +
                                  "VSQUSKILLSNBULDLEQFT\n" +
                                  "KBSEGDELWONKQLDGHLYN\n" +
                                  "RBZQLJCZZIQIKOYQYYBL\n" +
                                  "ULZFCFQLQCTVBCPEVGPQ\n" +
                                  "LPHTWILIVIGRXTXSDXMEIB\n" +
                                  "QZNDVMBTLJIDQUSLAJQK"));
  }

  @Test
  public void shouldConvertSkewedRealLifeCase() throws Exception {
    // given
    File input = new WordListLoader().getFileFromResource("/sample_case_wall_of_fame.jpg");

    // when
    String output = sut.convert(input);

    // then
    assertThat(output, is("Wall Of Fame"));
  }

  @Test
  public void shouldConvertIJRealLifeCase() throws Exception {
    // given
    File input = new WordListLoader().getFileFromResource("/sample_word_puzzle_extract.jpg");

    // when
    String output = sut.convert(input);

    // then
    assertThat(tidied(output), is("ZIC"));
  }

  //---- Helpers

  /**
   * Removes all spaces from a string.
   */
  private String tidied(String s) {
    return new WordListLoader().clean(s).toUpperCase();
  }
}
