package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.dictionary.EnglishLanguageDictionary;
import ch.retorte.wordsearchor.file.WordListLoader;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Integration test for the {@link WordSearchor}.
 */
public class WordSearchorIntegrationTest {

  @Test
  public void shouldFindMatches() throws Exception {
    // given
    EnglishLanguageDictionary dictionary = new EnglishLanguageDictionary();
    String input = "A B C W\n" +
                   "D E F I\n" +
                   "S U N R\n" +
                   "T R R E";

    // when
    WordSearchResult result = new WordSearchor(dictionary.wordList()).findMatchesIn(input);

    // then
    assertTrue(contains(result.getMatches(), "sun"));
    assertTrue(contains(result.getMatches(), "wire"));
  }

  @Test
  public void shouldProcessLargerTextSample() throws Exception {
    // given
    EnglishLanguageDictionary dictionary = new EnglishLanguageDictionary();
    List<String> input = new WordListLoader().asList("/sample_word_puzzle.txt");

    // when
    WordSearchResult result = new WordSearchor(dictionary.wordList()).findMatchesIn(input,3);

    // then
    assertTrue(contains(result.getMatches(), "development"));
    assertTrue(contains(result.getMatches(), "university"));
  }

  private boolean contains(Collection<WordSearchMatch> matches, String match) {
    for (WordSearchMatch m : matches) {
      if (m.getWord().equalsIgnoreCase(match)) {
        return true;
      }
    }
    return false;
  }
}
