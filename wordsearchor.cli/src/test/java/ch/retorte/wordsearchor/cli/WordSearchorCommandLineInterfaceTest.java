package ch.retorte.wordsearchor.cli;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link WordSearchorCommandLineInterface}.
 */
public class WordSearchorCommandLineInterfaceTest {

  //---- Fields

  private WordSearchorCommandLineInterface sut = spy(new WordSearchorCommandLineInterface());


  //---- Methods

  @Before
  public void setup() {
    doNothing().when(sut).printUsageMessage();
    doNothing().when(sut).quitProgram();
    doReturn(null).when(sut).getStreamInput();
  }

  @Test
  public void shouldParseArguments_printUsageOnHelpArgument() {
    // given
    String[] args = new String[] { "-h" };
    sut.createOptions();

    // when
    sut.parseArguments(args);

    // then
    verify(sut).printUsageMessage();
    verify(sut).quitProgram();
  }

  @Test
  public void shouldParseArguments_readShortProperties() {
    // given
    String[] args = new String[] { "-l", "EN", "-d", "myDict.txt", "-s", "4", "/abc/def" };
    sut.createOptions();

    // when
    sut.parseArguments(args);

    // then
    assertThat(sut.getDictionaryLanguage(), is("EN"));
    assertThat(sut.getDictionaryFilePath(), is("myDict.txt"));
    assertThat(sut.getMinimumWordSize(), is(4));
    assertThat(sut.getInputFilePath(), is("/abc/def"));
  }

  @Test
  public void shouldParseArguments_readLongProperties() {
    // given
    String[] args = new String[] { "--lang", "EN", "--dict", "myDict.txt", "--min-word-size", "4", "/abc/def" };
    sut.createOptions();

    // when
    sut.parseArguments(args);

    // then
    assertThat(sut.getDictionaryLanguage(), is("EN"));
    assertThat(sut.getDictionaryFilePath(), is("myDict.txt"));
    assertThat(sut.getMinimumWordSize(), is(4));
    assertThat(sut.getInputFilePath(), is("/abc/def"));
  }

  @Test
  public void shouldParseArguments_handleMissingInputFileArgument() {
    // given
    String[] args = new String[] { "--lang", "EN" };
    sut.createOptions();

    // when
    sut.parseArguments(args);

    // then
    assertThat(sut.getDictionaryLanguage(), is("EN"));
    assertThat(sut.getInputFilePath(), nullValue());
  }

}
