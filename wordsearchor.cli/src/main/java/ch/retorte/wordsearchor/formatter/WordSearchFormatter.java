package ch.retorte.wordsearchor.formatter;

import ch.retorte.wordsearchor.WordSearchMatch;
import ch.retorte.wordsearchor.WordSearchResult;
import ch.retorte.wordsearchor.matrix.PositionableMatrix;

import java.util.Iterator;
import java.util.StringJoiner;

/**
 * Formats results of a word search.
 */
public class WordSearchFormatter {

  //---- Static

  private static final String SPACE = " ";
  private static final String PLAIN = "\033[0;0m";
  private static final String BOLD = "\u001B[1m";

  private static final String NEWLINE = System.lineSeparator();


  //---- Fields

  private WordSearchResult wordSearchResult;


  //---- Constructor

  public WordSearchFormatter(WordSearchResult wordSearchResult) {
    this.wordSearchResult = wordSearchResult;
  }


  //---- Methods

  public String formattedInput() {
    StringJoiner lines = new StringJoiner(NEWLINE);
    for (String inputLine : wordSearchResult.getInput().split(NEWLINE)) {
      StringJoiner characters = new StringJoiner(SPACE);
      for (Character c : inputLine.toCharArray()) {
        characters.add(c.toString().toUpperCase());
      }
      lines.add(characters.toString());
    }

    return lines.toString();
  }

  public String formattedWordList() {
    StringJoiner lines = new StringJoiner(NEWLINE);
    for (WordSearchMatch m : wordSearchResult.getMatches()) {
      lines.add(m.toString());
    }
    return lines.toString();
  }

  public String formattedOutput() {
    StringJoiner lines = new StringJoiner(NEWLINE);

    Iterator<PositionableMatrix<WordSearchResult.MatchData>.ItemLine> itemLineIterator = wordSearchResult.getMatchLines();
    while (itemLineIterator.hasNext()) {
      PositionableMatrix<WordSearchResult.MatchData>.ItemLine line = itemLineIterator.next();
      StringJoiner characters = new StringJoiner(SPACE);
      for (PositionableMatrix.ItemHolder i : line.getItems()) {
        WordSearchResult.MatchData data = (WordSearchResult.MatchData) i.getData();

        String c = data.getCharacter().toString().toLowerCase();

        if (data.hasMatch()) {
          c = BOLD + c.toUpperCase();
        }
        else {
          c = PLAIN + ".";
        }

        characters.add(c);
      }
      lines.add(characters.toString());
    }

    return lines.toString() + PLAIN;
  }
}
