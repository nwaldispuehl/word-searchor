package ch.retorte.wordsearchor;

import ch.retorte.wordsearchor.cli.WordSearchorCommandLineInterface;

/**
 * This main class' only purpose is the invocation of the actual software.
 */
public class Main {

  public static void main(String... args) {
    new WordSearchorCommandLineInterface().startWith(args);
  }
}
