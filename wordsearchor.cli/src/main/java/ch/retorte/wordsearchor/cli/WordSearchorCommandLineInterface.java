package ch.retorte.wordsearchor.cli;

import ch.retorte.wordsearchor.WordSearchResult;
import ch.retorte.wordsearchor.WordSearchor;
import ch.retorte.wordsearchor.dictionary.EnglishLanguageDictionary;
import ch.retorte.wordsearchor.dictionary.FileBasedLanguageDictionary;
import ch.retorte.wordsearchor.dictionary.GermanLanguageDictionary;
import ch.retorte.wordsearchor.formatter.WordSearchFormatter;
import ch.retorte.wordsearchor.ocr.ImageConverter;
import ch.retorte.wordsearchor.spi.LanguageDictionary;
import org.apache.commons.cli.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Command line front end for the word searchor solver engine.
 */
public class WordSearchorCommandLineInterface {

  //---- Static

  private static final String HELP_OPTION_SHORT = "h";
  private static final String HELP_OPTION_LONG = "help";

  private static final String LANGUAGE_OPTION_SHORT = "l";
  private static final String LANGUAGE_OPTION_LONG = "lang";

  private static final String DICTIONARY_OPTION_SHORT = "d";
  private static final String DICTIONARY_OPTION_LONG = "dict";

  private static final String MINIMUM_WORD_SIZE_OPTION_SHORT = "s";
  private static final String MINIMUM_WORD_SIZE_OPTION_LONG = "min-word-size";

  private static final String VERBOSE_OPTION_SHORT = "v";
  private static final String VERBOSE_OPTION_LONG = "verbose";

  private static final String ENGLISH = "en";
  private static final String GERMAN = "de";

  private static final Map<String, Class<? extends LanguageDictionary>> DICTIONARIES = new HashMap<>();

  static {
    DICTIONARIES.put(ENGLISH, EnglishLanguageDictionary.class);
    DICTIONARIES.put(GERMAN, GermanLanguageDictionary.class);
//    DICTIONARIES.put("fr", FrenchLanguageDictionary.class);
//    DICTIONARIES.put("es", SpanishLanguageDictionary.class);
  }

  private static final String UNKNOWN_TYPE = "unknown";
  private static final String TEXT_TYPE_PREFIX = "text";
  private static final String IMAGE_TYPE_PREFIX = "image";


  //---- Fields

  private Options cliOptions;

  private String inputFilePath;
  private String dictionaryLanguage = ENGLISH;
  private String dictionaryFilePath = null;
  private int minimumWordSize = 4;
  private boolean verboseOutput = false;

  private String streamInput;


  //---- Methods

  public void startWith(String[] args) {
    createOptions();
    parseArguments(args);

    try {
      LanguageDictionary dictionary = getDictionaryWith(dictionaryFilePath, dictionaryLanguage);
      String input = prepareInputWith(streamInput, inputFilePath);


      WordSearchor wordSearchor = new WordSearchor(dictionary.wordList());
      WordSearchResult result = wordSearchor.findMatchesIn(input, minimumWordSize);
      createOutputWith(result);
    }
    catch (Exception e) {
      quitProgramWithError(e.getMessage());
    }
  }

  void createOptions() {
    cliOptions = new Options();
    cliOptions.addOption(HELP_OPTION_SHORT, HELP_OPTION_LONG, false, "Displays the help screen.");
    cliOptions.addOption(LANGUAGE_OPTION_SHORT, LANGUAGE_OPTION_LONG, true, "Which language dictionary to use. Supports 'en', 'de', 'fr', 'es'. Defaults to 'en'.");
    cliOptions.addOption(DICTIONARY_OPTION_SHORT, DICTIONARY_OPTION_LONG, true, "Path to a custom dictionary in a text file. One word per line.");
    cliOptions.addOption(MINIMUM_WORD_SIZE_OPTION_SHORT, MINIMUM_WORD_SIZE_OPTION_LONG, true, "Minimum length (inclusive) of words to look for. Defaults to 4.");
    cliOptions.addOption(VERBOSE_OPTION_SHORT, VERBOSE_OPTION_LONG, false, "Shows more details in the output.");
  }

  void parseArguments(String[] args) {
    CommandLineParser parser = new PosixParser();
    CommandLine cmd;
    try {
      cmd = parser.parse(cliOptions, args);
    } catch (ParseException e) {
      quitProgramWithError(e.getMessage());
      return;
    }

    try {
      if (cmd.hasOption(HELP_OPTION_SHORT)) {
        printHelpAndQuit();
        return;
      }

      if (cmd.hasOption(LANGUAGE_OPTION_SHORT)) {
        dictionaryLanguage = cmd.getOptionValue(LANGUAGE_OPTION_SHORT);
      }

      if (cmd.hasOption(DICTIONARY_OPTION_SHORT)) {
        dictionaryFilePath = cmd.getOptionValue(DICTIONARY_OPTION_SHORT);
      }

      if (cmd.hasOption(MINIMUM_WORD_SIZE_OPTION_SHORT)) {
        minimumWordSize = Integer.valueOf(cmd.getOptionValue(MINIMUM_WORD_SIZE_OPTION_SHORT));
      }

      if (cmd.hasOption(VERBOSE_OPTION_SHORT)) {
        verboseOutput = true;
      }

      inputFilePath = getLastArgumentIn(cmd);

      if (inputFilePath == null) {
        streamInput = getStreamInput();
      }

    } catch (Exception e) {
      printHelpAndQuit();
    }

    if (inputFilePath == null && streamInput == null) {
      printHelpAndQuit();
    }
  }

  private void printHelpAndQuit() {
    printUsageMessage();
    quitProgram();
  }

  private String getLastArgumentIn(CommandLine cmd) {
    if (cmd.getArgList().isEmpty()) {
      return null;
    }
    else {
      return (String) cmd.getArgList().get(cmd.getArgList().size() - 1);
    }
  }

  String getStreamInput() {
    List<String> lines = new LinkedList<>();
    Scanner in = new Scanner(System.in);
    while (in.hasNextLine()) {
      lines.add(in.nextLine());
    }

    if (lines.isEmpty()) {
      return null;
    }
    else {
      return String.join(System.lineSeparator(), lines);
    }
  }

  private LanguageDictionary getDictionaryWith(String dictionaryFilePath, String dictionaryLanguage) throws Exception {
      if (has(dictionaryFilePath)) {
        return loadDictionaryFrom(dictionaryFilePath);
      }
      else {
        return loadBuiltInDictionaryWith(dictionaryLanguage);
      }
  }

  private boolean has(String s) {
    return s != null;
  }

  private LanguageDictionary loadDictionaryFrom(String dictionaryFilePath) throws FileNotFoundException {
    return new FileBasedLanguageDictionary(new File(dictionaryFilePath));
  }

  private LanguageDictionary loadBuiltInDictionaryWith(String dictionaryLanguage) throws Exception {
    if (!DICTIONARIES.containsKey(dictionaryLanguage)) {
      throw new IllegalArgumentException("Unknown dictionary: " + dictionaryLanguage);
    }

    Class<? extends LanguageDictionary> languageDictionaryClass = DICTIONARIES.get(dictionaryLanguage);
    return languageDictionaryClass.getConstructor().newInstance();
  }
  
  private String prepareInputWith(String streamInput, String inputFilePath) throws IOException, URISyntaxException {
    if (has(streamInput)) {
      return streamInput;
    }
    else {
      if (isTextFile(inputFilePath)) {
        return loadTextFrom(inputFilePath);
      }
      else if (isImageFile(inputFilePath)) {
        return performOcrOn(inputFilePath);
      }
      else {
        throw new IllegalArgumentException("Input file needs to be either text or image. Type of provided argument: " + getTypeFrom(inputFilePath));
      }
    }
  }

  private boolean isTextFile(String inputFilePath) {
    return getTypeFrom(inputFilePath).startsWith(TEXT_TYPE_PREFIX);
  }

  private String getTypeFrom(String path) {
    try {
      return Files.probeContentType(Paths.get(path));
    } catch (IOException e) {
      return UNKNOWN_TYPE;
    }
  }

  private String loadTextFrom(String inputFilePath) throws IOException {
    return new String(Files.readAllBytes(Paths.get(inputFilePath)));
  }

  private boolean isImageFile(String inputFilePath) {
    return getTypeFrom(inputFilePath).startsWith(IMAGE_TYPE_PREFIX);
  }

  private String performOcrOn(String inputFilePath) throws IOException, URISyntaxException {
    return new ImageConverter().convert(new File(inputFilePath));
  }

  private void createOutputWith(WordSearchResult wordSearchResult) {
    WordSearchFormatter formatter = new WordSearchFormatter(wordSearchResult);

    if (verboseOutput) {
      print("- Input:");
      print(formatter.formattedInput());
      print();

      print("- Word list (" + wordSearchResult.size() + " hits):");
      print(formatter.formattedWordList());
      print();

      print("- Output:");
    }

    print(formatter.formattedOutput());
    print();
  }

  private void printProgramTitle() {
    print("Word Searchor -- Solver for word search puzzles.");
    print();
    print("Solves classical word puzzles - where words have to be found in a block of");
    print("letters - with text or an image as input data. Has already some default ");
    print("dictionaries included, custom word lists can be provided");
    print();
    print("Input has to be present in block form, e.g.");
    print();
    print("    A B C");
    print("    D E F");
    print();
  }

  private void print() {
    print("");
  }

  private void print(String text) {
    System.out.println(text);
  }

  void printUsageMessage() {
    printProgramTitle();

    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("wordsearchor [OPTIONS] [INPUTFILE]\n" +
        "The INPUTFILE can either be a text or image file,\n" +
        "or none. Then input is read from stdin.",
        cliOptions);
  }

  void quitProgram() {
    System.exit(0);
  }

  private void quitProgramWithError(String errorMessage) {
    printProgramTitle();
    System.out.println();
    System.err.println("ERROR: " + errorMessage);
    System.exit(1);
  }

  //---- Argument getters

  String getInputFilePath() {
    return inputFilePath;
  }

  String getDictionaryLanguage() {
    return dictionaryLanguage;
  }

  String getDictionaryFilePath() {
    return dictionaryFilePath;
  }

  int getMinimumWordSize() {
    return minimumWordSize;
  }
}
