package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.file.WordListLoader;
import ch.retorte.wordsearchor.spi.LanguageDictionary;

/**
 * Implementation of the {@link LanguageDictionary} which provides words of the german language.
 */
public class GermanLanguageDictionary extends InputStreamBasedLanguageDictionary {

  //---- Static

  private static final String WORD_LIST_FILE_NAME = "/german_wordlist_derewo_100000.txt";


  //---- Constructor

  public GermanLanguageDictionary() throws Exception {
    super(new WordListLoader().getInputStreamFromResource(WORD_LIST_FILE_NAME));
  }

}
