package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.spi.LanguageDictionary;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link GermanLanguageDictionary}.
 */
public class GermanLanguageDictionaryTest {

  @Test
  public void shouldProduceGermanWordList() throws Exception {
    // given
    LanguageDictionary sut = new GermanLanguageDictionary();

    // when
    List<String> wordList = sut.wordList();

    // then
    assertThat(wordList.size(), is(100000));
    Iterator<String> iterator = wordList.iterator();
    assertThat(iterator.next(), is("der"));
    assertThat(iterator.next(), is("die"));
    assertThat(iterator.next(), is("und"));
  }
}
