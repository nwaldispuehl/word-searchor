package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.file.WordListLoader;
import ch.retorte.wordsearchor.spi.LanguageDictionary;

/**
 * Implementation of the {@link LanguageDictionary} which provides words of the english language.
 */
public class EnglishLanguageDictionary extends InputStreamBasedLanguageDictionary {

  //---- Static

  private static final String WORD_LIST_FILE_NAME = "/english_lemmas_1998.txt";


  //---- Constructor

  public EnglishLanguageDictionary() throws Exception {
    super(new WordListLoader().getInputStreamFromResource(WORD_LIST_FILE_NAME));
  }

}
