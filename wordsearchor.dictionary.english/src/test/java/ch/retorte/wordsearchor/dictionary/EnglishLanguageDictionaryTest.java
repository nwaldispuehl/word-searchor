package ch.retorte.wordsearchor.dictionary;

import ch.retorte.wordsearchor.spi.LanguageDictionary;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for the {@link EnglishLanguageDictionary}.
 */
public class EnglishLanguageDictionaryTest {

  @Test
  public void shouldProduceEnglishWordList() throws Exception {
    // given
    LanguageDictionary sut = new EnglishLanguageDictionary();

    // when
    List<String> wordList = sut.wordList();

    // then
    assertThat(wordList.size(), is(40568));
    Iterator<String> iterator = wordList.iterator();
    assertThat(iterator.next(), is("a"));
    assertThat(iterator.next(), is("an"));
    assertThat(iterator.next(), is("A-bomb"));
  }
}
